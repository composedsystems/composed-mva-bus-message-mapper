## [0.1.16] - 2020-03-27
fixed a bug were the dialog saying that a library wasn't explictily in the project was popping up incorrectly.

## [0.1.15] - 2020-03-16
Adding ability to navigate to a vi in the project. Also added a warning if the lvlib is in dependencies vs explicitliy added to the project. 

## [0.1.14] - 2020-03-16
Doubble clicking on an actor navigates to that actor in the LabVIEW project.

## [0.1.13] - 2020-03-14
Fixed a bug where if you double click on empty space in a broadcaster or receier list, the program errors out.
Added the Actor Centric View.
Added the Broadcasting Actor to the message centric view.

## [0.1.12] - 2020-03-04
Adding support for vims. 

## [0.1.11] - 2020-03-02
Adding control terminal as source in list.

## [0.1.10] - 2020-03-02
Added traversal algorithm for build array.

## [0.1.9] - 2020-2-25
Added "Request Data.vi" 

## [0.1.8] - 2020-2-24
added all of the "bind to" types and added register for multiple messages. Also now i'm flashing both the source and sink of the data.

## [0.1.7] - 2020-2-23
Made custom searching injectable.
added some test code.
can detect arrays of strings passed into for loops.

need to add other mva functions.
filter out stuff from the mva framework
maybe handle ccat.

## [0.1.6] - 2020-02-21
Added object highlighting when you double click on a broadcaster or receiver. 

-Added cache regeneration if unflattening the cache fails. 

## [0.1.1] - 2020-02-10
-Something was messed up with the suppored LabVIEW Version.

## [0.1.0] - 2020-02-10
-Initial alpha prelease. 