# MVA Bus Message Mapper

Displays a map of bus message senders and receivers for an MVA application. After importing this GPackage, in your project navigate to gpm packages\mva-message-mapper\Main.vi and run. 

Select "Load Cache". 

You can double click on a Reciver or Broadcaster to open the block diagram. The specific node will be highlighted.